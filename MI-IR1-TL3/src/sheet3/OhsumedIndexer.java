package sheet3;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import edu.uci.ics.jung.graph.Graph;

public class OhsumedIndexer {

	// FIELD NAMES:
	// internal ID (.I)
	public static final String ID = "docid";
	// title of the entry (.T)
	public static final String TITLE = "title";
	// abstract/content (.W)
	public static final String CONTENT = "content";
	// mesh terms assigned by humans
	public static final String MESH = "mesh";
	
	// analyzer
	public Analyzer analyzer = null; 

	// index writer
	public IndexWriter writer;

	// determines which analyzer should be used
	public static boolean useStandardAnalyzer = true;
	
	// constructor
	public OhsumedIndexer(String indexDir, Analyzer analyzer) throws IOException {
		Directory dir = FSDirectory.open(new File(indexDir));
		
		this.analyzer = analyzer;
		
		IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_45,
				analyzer);

		writer = new IndexWriter(dir, iwc); // 3 modified
	}

	// main method for testing
	public static void main(String[] args) throws Exception {

		String indexDir = null;// 1
		String dataDir = "data/ohsumed/docs"; // 2

		Analyzer analyzer = null;
		
		if (useStandardAnalyzer){
			indexDir = "data/idx_ohsumed_std";
			analyzer = new StandardAnalyzer(Version.LUCENE_45);
		} else {// use MyStemAnalyzer
			indexDir = "data/idx_ohsumed_my";
			analyzer = new MyStemAnalyzer();
		}
		
		long start = System.currentTimeMillis();
		OhsumedIndexer indexer = new OhsumedIndexer(indexDir, analyzer);
		int numIndexed;
		try {
			numIndexed = indexer.index(dataDir);
		} finally {
			indexer.close();
		}
		long end = System.currentTimeMillis();

		System.out.println("Indexing " + numIndexed + " files took "
				+ (end - start) + " milliseconds");
	}

	// as before, nothing new :-)
	public int index(String dataDir) throws Exception {

		File[] files = new File(dataDir).listFiles();

		for (File f : files) {
			if (f.getName().startsWith("ohsumed.")) {
				indexFile(f);
			}
		}

		return writer.numDocs(); // 5
	}

	// as before, nothing new :-)
	public void close() throws IOException {
		writer.close(); // 4
	}

	// Do the indexing! (see exercise 4.1)
	public void indexFile(File file) throws Exception {
		
		System.out.println("Indexing " + file.getCanonicalPath());
		
		// for scanning the file
		Scanner scanner = new Scanner(file);
		
		// temporary collection variables
		int docId = -1;
		String meshVars = ""; 
		String title = ""; 
		String contents = "";
		
		// scan document line by line
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			// get relevant information
			if(line.startsWith(".I")){
				String[] temp = line.split(" ");
				docId = Integer.parseInt(temp[1]);
			} else if(line.startsWith(".M")) {
				meshVars = scanner.nextLine();
			} else if(line.startsWith(".T")){
				title = scanner.nextLine();
			} else if(line.startsWith(".W")){
				contents = scanner.nextLine();
			}
			
			// check if all necessary data is collected - includes valid document id, a title and the mesh terms
			if((docId > -1) && (!meshVars.isEmpty()) && (!title.isEmpty())){
				// information output
				System.out.println("FOUND DOCUMENT WITH ID " + docId + " AND TITLE " + title);
				// System.out.println("MESH-VARS FOR DOCUMENT: " + meshVars);
				// System.out.println("TITLE FOR DOCUMENT: " + title);
				// System.out.println("CONTENT: " + contents);
				
				// create new lucene document and add it to index writer
				Document doc = new Document();
				doc.add(new IntField(ID, docId, Field.Store.YES));
				doc.add(new StringField(MESH, meshVars, Field.Store.YES));
				doc.add(new StringField(TITLE, title, Field.Store.YES));
				doc.add(new TextField(CONTENT, contents, Field.Store.YES));
				writer.addDocument(doc);
				
				// reset temporary variables
				docId = -1;
				meshVars = "";
				title = "";
				contents = "";
			}
		}
		
		scanner.close();
	}

	// Modify input text according to TW-IDF model (see exercise 5.6)
	public String modifyTextContent(String content) {		
		List<String> splittedText = Windowing.tokenizeString(analyzer, content);
		Graph<String,Integer> twgraph = Windowing.generateTWGraph(splittedText);
		content = Windowing.generateTextRepresentation(twgraph);
		return content;
	}

}

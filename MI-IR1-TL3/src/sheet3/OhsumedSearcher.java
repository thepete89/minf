package sheet3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.LMJelinekMercerSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class OhsumedSearcher {

	// use standard analyzer or not
	public static boolean useStandardAnalyzer = false;

	// path to the file with the queries
	public static String path2queries = "data/ohsumed/queries2.txt";

	// main method for testing
	public static void main(String[] args) throws IllegalArgumentException,
			IOException, ParseException {

		String indexDir = null;// 1
		String outName = null;
		Analyzer analyzer = null;

		if (useStandardAnalyzer) {
			indexDir = "data/idx_ohsumed_std";
			analyzer = new StandardAnalyzer(Version.LUCENE_45);
			outName = "std";
		} else {// use MyStemAnalyzer
			indexDir = "data/idx_ohsumed_my";
			analyzer = new MyStemAnalyzer();
			outName = "my";
		}

		Similarity[] sims = new Similarity[] {
				new LMJelinekMercerSimilarity(0.2f),
				new LMDirichletSimilarity(), new BM25Similarity(),
				new DefaultSimilarity(),
				/*new MySimilarity(TF.as_is, IDF.as_is),
				new MySimilarity(TF.none, IDF.as_is),
				new MySimilarity(TF.as_is, IDF.none),
				new MySimilarity(TF.absolute_tf, IDF.as_is),
				new MySimilarity(TF.log, IDF.as_is),
				new MySimilarity(TF.log, IDF.none) */};

		for (int i = 0; i < sims.length; i++) {
			Similarity sim = sims[i];
			StringBuilder builder = search(indexDir, sim, analyzer);
			System.err.println("ohsumed-" + sim.toString() + "-"
					+ analyzer.toString() + ".trec");
			FileWriter fw = new FileWriter(new File("data/ohsumed-" + sim.toString()
					+ "-" + outName + ".trec"));
			fw.write(builder.toString());
			fw.close();
		}
	}

	// implement the search on multiple fields (see exercise 4.3 and following)
	public static StringBuilder search(String indexDir, Similarity sim,
			Analyzer analyzer) throws IOException, ParseException {
		
		System.out.println("PROCESSING QUERIES FROM " + path2queries + " WITH " + sim.toString() + " SIMILARITY AND " + analyzer.toString() + " ANALYZER");
		
		StringBuilder builder = new StringBuilder();
		
		// create query parser
		String[] fields = {OhsumedIndexer.ID, OhsumedIndexer.MESH, OhsumedIndexer.TITLE, OhsumedIndexer.CONTENT};
		MultiFieldQueryParser parser = new MultiFieldQueryParser(Version.LUCENE_45, fields, analyzer);
		
		// set up searcher
		IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(
				indexDir)));
		IndexSearcher is = new IndexSearcher(reader);
		is.setSimilarity(sim);

		// temporary variables
		int queryId = -1;
		String patientInfo = "";
		String queryString = "";

		// load query file
		Scanner scanner = new Scanner(new File(path2queries));
		
		// parse query file
		while (scanner.hasNextLine()) {
			// same approach as in OhsumedIndexer
			String line = scanner.nextLine().trim();
			
			if(line.startsWith(".I")){
				String[] temp = line.split("  ", 2);
				queryId = Integer.parseInt(temp[1]);
			} else if(line.startsWith(".B")){
				patientInfo = scanner.nextLine().trim();
			} else if(line.startsWith(".W")){
				queryString = scanner.nextLine().trim();
			}
			
			// check if querydata is complete
			if(queryId > -1 && (!patientInfo.isEmpty()) && (!queryString.isEmpty())){
				// some preformatting
				queryString = queryString.replaceAll("/", " ");
				
				// construct query and do a search, limited to 1000 results
				Query query = parser.parse(queryString);
				TopDocs hits = is.search(query, 1000);
				// System.out.println("FOUND " + hits.totalHits + " DOCUMENTS FOR QUERY WITH ID " + queryId + " - BUILDING TRAC DATA FOR FIRST 1000 RESULTS");
				
				for(ScoreDoc doc : hits.scoreDocs){
					builder.append(queryId + " 0 " + is.doc(doc.doc).get(OhsumedIndexer.ID) + " 1 " + doc.score + " query-" + queryId + "\n");
				}
				
				// reset variables
				queryId = -1;
				patientInfo = "";
				queryString = "";
			}
			
		}

		scanner.close();
		System.out.println("DONE");

		return builder;
	}
}

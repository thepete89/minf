package sheet3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class OhsumedHelper {

	// method for reading the query texts -> use it!
	public static List<String> readQueries(String filename)
			throws FileNotFoundException {

		List<String> queryTexts = new LinkedList<String>();

		Scanner scanner = new Scanner(new File(filename));
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine().trim();

			if (line.startsWith(".W")) {
				String queryText = scanner.nextLine().trim();
				queryText = queryText.replaceAll("/", " ");
				queryTexts.add(queryText);
			}
		}

		scanner.close();

		return queryTexts;
	}

	// method for generating the qrel file (see exercise 4.2)
	public static StringBuilder generateQRels(String filename)
			throws IOException {

		StringBuilder builder = new StringBuilder();
		Scanner scanner = new Scanner(new File(filename));

		// scan document line by line
		while (scanner.hasNextLine()) {
			// get line data and split it at tabulator, limiting to two values
			// as there should not be more
			String[] data = scanner.nextLine().split("\\t", 2);
			// append retrieved data to string builder
			builder.append(data[0] + " 0 " + data[1] + " 1\n");
		}

		scanner.close();

		return builder;
	}

	// main method for testing
	public static void main(String[] args) {
		StringBuilder builder1 = null, builder2 = null;
		try {
			// generate qrels files: TODO: change paths
			builder1 = OhsumedHelper.generateQRels("data/ohsumed/drel.i");
			builder2 = OhsumedHelper.generateQRels("data/ohsumed/pdrel.i");

			if (builder1 == null || builder2 == null) {
				throw new RuntimeException("Work on the TODOs!");
			}

			FileWriter fw = new FileWriter(new File("data/ohsumed-d.qrel"));
			fw.write(builder1.toString());
			fw.close();

			fw = new FileWriter(new File("data/ohsumed-pd.qrel"));
			fw.write(builder2.toString());
			fw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

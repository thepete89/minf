package sheet3;

import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JFrame;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;

public class Windowing {

	// the window size (see paper)
	public static final int WINDOW_SIZE = 3;

	// input text
	// public static String exampleText =
	// "Information retrieval is the activity of obtaining information resources relevant to an information need from a collection of information resources.";
	public static String exampleText = "rock music is a genre of popular music that originated as rock_and_roll in the US in the 1950s that has its roots in 1940s and 1950s rock_and_roll, itself heavily influenced by country music.";

	// main method for testing
	public static void main(String[] args) {
		
		// analyze text
		List<String> splittedText = Windowing.tokenizeString(
				new StandardAnalyzer(Version.LUCENE_45
				/* CharArraySet.EMPTY_SET */), exampleText);

		// generate TW graph (see paper)
		Graph<String, Integer> graph = generateTWGraph(splittedText);

		// generate text representation suitable for lucene
		String text2index = generateTextRepresentation(graph);
		System.out.println(text2index);
		
		// visualize graph
		vizualizeGraph(graph);
	}

	// use Lucene analyzer for tokenizing and filtering (see exercise 5.3)
	public static List<String> tokenizeString(Analyzer analyzer, String text) {
		List<String> result = null;
		
		TokenStream tokenStream;
		try {
			tokenStream = analyzer.tokenStream("data", text);
			CharTermAttribute termAtt = tokenStream.addAttribute(CharTermAttribute.class);
			tokenStream.reset();
			result = new ArrayList<>();
			
			while(tokenStream.incrementToken()){
				result.add(termAtt.toString());
			}
			
			tokenStream.end();
			tokenStream.close();
			
		} catch (IOException e) {
			throw new RuntimeException("Could not tokenize text!", e);
		}
		return result;
	}

	// generating the word graph (see exercise 5.4)
	public static Graph<String, Integer> generateTWGraph(List<String> splittedText) {

		DirectedSparseGraph<String, Integer> graph = new DirectedSparseGraph<>();

		// as described in twidf.pdf we use a window size of 3
		int windowSize = 3;
		// just counting edges up, graph implementation should do the rest
		int edge = 0;
		
		for (int pos = 0; pos < splittedText.size(); pos++) {
			String baseToken = splittedText.get(pos);
			System.out.println("CURRENT POSITION: " + pos + " - TOKEN: " + baseToken);
			// find following tokens within windowSize
			for (int i = pos+1; i < pos+windowSize; i++) {
				// check if we are in list bounds
				if(i < splittedText.size()){
					String followingToken = splittedText.get(i);
					System.out.println("NEIGHBOUR: " + i + " TOKEN: " + followingToken + " GRAPH EDGE: " + edge);
					// add edge to graph with baseToken as origin vertex and followingToken as target vertex
					graph.addEdge(edge, baseToken, followingToken);
					edge++;
				}
			}
		}
		
		return graph;
	}

	// generating the alternative text representation (see exercise 5.5)
	public static String generateTextRepresentation(Graph<String, Integer> graph) {
		String text2index = "";

		// simple approach according to paper: go trough graph vertice by vertice and determine its inDegree
		// repeat every word as many times its inDegree
		Collection<String> vertices = graph.getVertices();
		for (String vertice : vertices) {
			int degree = graph.inDegree(vertice);
			for (int i = 0; i < degree; i++) {
				text2index += vertice + " ";
			}
		}
		
		return text2index;
	}
	
	// a method for visualizing a graph, feel free to change it if necessary!
	public static void vizualizeGraph(Graph<String, Integer> webgraph) {

		Layout<String, Integer> layout = new KKLayout<String, Integer>(webgraph);
		layout.setSize(new Dimension(700, 700));
		BasicVisualizationServer<String, Integer> viz = new BasicVisualizationServer<String, Integer>(
				layout);
		viz.setPreferredSize(new Dimension(750, 750));
		viz.getRenderContext().setVertexLabelTransformer(
				new ToStringLabeller<String>());
		// viz.getRenderContext().setEdgeLabelTransformer(new
		// ToStringLabeller<Integer>());

		JFrame frame = new JFrame("Beispiel zum TW-IDF Modell");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(viz);
		frame.pack();
		frame.setVisible(true);
	}

}

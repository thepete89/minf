package sheet3;

import org.apache.lucene.search.similarities.DefaultSimilarity;

// class for computing TF-IDF similarities
public class MySimilarity extends DefaultSimilarity {

	// how should TF be handled
	public enum TF {
		as_is, none, absolute_tf, log
	};

	// how should IDF be handled
	public enum IDF {
		as_is, none
	};

	// the current TF option
	public TF currentTF = null;
	
	// the current IDF option
	public IDF currentIDF = null;

	// Constructor
	public MySimilarity(TF currentTF, IDF currentIDF) {
		super();
		this.currentTF = currentTF;
		this.currentIDF = currentIDF;
	}

	@Override
	public float tf(float freq) {
		if(currentTF.equals(TF.as_is)){
			return super.tf(freq);
		} else if(currentTF.equals(TF.none)){
			// do nothing
		} else if(currentTF.equals(TF.absolute_tf)){
			// how to calculate this?
		} else if(currentTF.equals(TF.log)){
			// same here
		}
		return -1f;
	}

	@Override
	public float idf(long docFreq, long numDocs) {
		if(currentIDF.equals(IDF.as_is)){
			return super.idf(docFreq, numDocs);
		}
		return -1f;	
	}

	@Override
	public String toString() {
		return "TFIDF(" + currentTF + "-" + currentIDF + ")";
	}

}

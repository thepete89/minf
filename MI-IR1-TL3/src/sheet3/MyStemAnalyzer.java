package sheet3;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.util.Version;

public class MyStemAnalyzer extends Analyzer {

	@Override
	protected TokenStreamComponents createComponents(String fieldName,
			Reader reader) {
		
		Tokenizer source = new StandardTokenizer(Version.LUCENE_45,reader);
		TokenStream filter = new LowerCaseFilter(Version.LUCENE_45, source);
		filter = new StopFilter(Version.LUCENE_45, filter, StopAnalyzer.ENGLISH_STOP_WORDS_SET);
		filter = new PorterStemFilter(source);
		
		return new TokenStreamComponents(source, filter);
	}
}

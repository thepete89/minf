package sheet1.exercise3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

public class ContentFinder {

	// Character indicating a Tag
	public static final String TAG_INDICATOR = "#";

	/*
	 * Content of a HTML file with tags replaced and preprocessing being done.
	 */
	public static String[] content = null;

	// Main method for testing
	public static void main(String[] args) throws IOException {

		ContentFinder contentFinder = new ContentFinder();

		// Part 3.1
		String text = contentFinder
				.extractBody("http://www.uni-bamberg.de/minf/leistungen/studium/bonuspunkte/");
		System.out.println(text);

		// Part 3.2
		text = contentFinder.preprocessBody(text);
		System.out.println(text);

		// Part 3.3
		String bitstring = contentFinder.createBitString(text);
		System.out.println(bitstring);

		// Part 3.4
		int[] result = contentFinder.identifyMainText(bitstring);

		if (result == null) {
			throw new RuntimeException("Work on the TODOs!");
		}

		// print the result
		System.out.println("Result:");
		System.out.println("left = " + result[0] + " right = " + result[1]
				+ " maxsum:" + result[2]);

		for (int i = result[0]; i <= result[1]; i++) {
			System.out.print(content[i] + " ");
		}
	}

	// Part 3.1 (see exercise)
	public String extractBody(String urlstring) {
		try {
			// open url
			URL siteUrl = new URL(urlstring);
			BufferedReader input = new BufferedReader(new InputStreamReader(
					siteUrl.openStream()));
			
			// reading content
			// System.out.println("------ HTML CONTENT OF: " + urlstring + " ------");
			String data = "";
			String inputLine;
			while((inputLine = input.readLine()) != null){
				// System.out.println(inputLine);
				data += inputLine;
			}
			input.close();
			// System.out.println("------ DONE ------");
			
			// extracting body
			String body = StringUtils.substringBetween(data, "<body>", "</body>");
			
			/*
			System.out.println("------ BODY EXTRACTED ------");
			System.out.println(body);
			System.out.println("------ DONE ------");
			*/
			// escaping
			String bodyUnescaped = StringEscapeUtils.unescapeHtml4(body);
			/*
			System.out.println("------ UNESCAPED DATA ------");
			System.out.println(bodyUnescaped);
			System.out.println("------ DONE ------");
			*/
			return bodyUnescaped;
				
		} catch (MalformedURLException e) {
			throw new RuntimeException("URL was malformed!");
		} catch (IOException e) {
			throw new RuntimeException("Error while reading contents");
		}
	}

	// Part 3.2 (see exercise)
	public String preprocessBody(String text) {
		
		// strip script tags
		Pattern patternScript = Pattern.compile("<script.*?>(.|\\s)*?</script>");
		Matcher matchScript = patternScript.matcher(text);
		String scriptStripped = matchScript.replaceAll(" ");
		
		// strip noscript
		Pattern patternNoscript = Pattern.compile("<noscript.*?>(.|\\s)*?</noscript>");
		Matcher matchNoscript = patternNoscript.matcher(scriptStripped);
		scriptStripped = matchNoscript.replaceAll(" ");
		/*
		System.out.println("------ REMOVED SCRIPT TAGS ------");
		System.out.println(scriptStripped);
		System.out.println("------ DONE ------");
		*/
		// replace all whitespaces (two or more spaces, newlines, tabs)
		Pattern patternRemoveNewline = Pattern.compile("\\s+");
		Matcher matchRemoveNewline = patternRemoveNewline.matcher(scriptStripped);
		String noSpace = matchRemoveNewline.replaceAll(" ");
		/*
		System.out.println("------ WHITESPACES REMOVED ------");
		System.out.println(noSpace);
		System.out.println("------ DONE ------");
		*/
		// replace html-tags
		Pattern patternReplaceTags = Pattern.compile("<.*?>");
		Matcher matchReplaceTags = patternReplaceTags.matcher(noSpace);
		String tagsReplaced = matchReplaceTags.replaceAll(" "+TAG_INDICATOR+" ");
		/*
		System.out.println("------ TAGS REPLACED ------");
		System.out.println(tagsReplaced);
		System.out.println("------ DONE ------");
		*/
		// remove dots
		Pattern patternRemoveDot = Pattern.compile("[.]");
		Matcher matchRemoveDot = patternRemoveDot.matcher(tagsReplaced);
		String finished = matchRemoveDot.replaceAll("");
		/*
		System.out.println("------ DOTS REMOVED ------");
		System.out.println(finished);
		System.out.println("------ DONE ------");
		*/
		return finished;
	}

	// Part 3.3 (see exercise)
	public String createBitString(String input) {
		String bitstring = "";
		
		content = StringUtils.split(input);
		
		for (int i = 0; i < content.length; i++) {
			String chunk = content[i];
			if(chunk.contains(TAG_INDICATOR)){
				bitstring += "1";
			} else {
				bitstring += "0";
			}
		}
		
		return bitstring;
	}

	// Part 3.4 in O(N^3) (see exercise)
	public int[] identifyMainText(String bitstring) {
		
		int maxTokens = 0;
		int[] result = {0, 0, 0};
		for (int l = 0; l < bitstring.length(); l++) {
			for (int u = l; u < bitstring.length(); u++) {
				int tokenCount = 0;
				for (int i = l; i <= u; i++) {
					// as this kind of approach doesn't like 0s and 1s we define:
					// if current position is a 0: add 1, else subtract 1
					if(String.valueOf(bitstring.charAt(i)).equals("0")){
						tokenCount++;
					} else {
						tokenCount--;
					}
				}
				//maxSoFar = Math.max(maxSoFar, sum);
				if(maxTokens < tokenCount){
					// found new maximum
					maxTokens = tokenCount;
					result[0] = l;
					result[1] = u;
					result[2] = tokenCount;
				}
			}
		}
		
		return result;
	}
}

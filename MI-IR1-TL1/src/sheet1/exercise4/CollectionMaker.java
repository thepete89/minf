package sheet1.exercise4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CollectionMaker {

	// total number of sonetts
	public static int numberOfSonnets = 154;

	// Main method for testing
	public static void main(String[] args) {
		String[] documents = readDocuments("data/collection.txt");

		System.out.println("Shakespeare's Sonnets");
		for (int i = 0; i < documents.length; i++) {
			System.out.println(i + "> " + documents[i]);
		}
	}

	// Exercise 4.1
	public static String[] readDocuments(String string) {
		
		Scanner scanner;
		String result[] = new String[numberOfSonnets];
		try {
			scanner = new Scanner(new File(string));
			int i = 0;
			
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				result[i] = line;
				i++;
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			System.err.println("File " + string + " not found!");
		}
				
		return result;
	}
}

package sheet2.exercise9;

import java.io.IOException;
import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.util.Version;

public class MyStemAnalyzer extends Analyzer {

	private Version version;
	private int maxTokenLength = StandardAnalyzer.DEFAULT_MAX_TOKEN_LENGTH;

	public MyStemAnalyzer(Version version) {
		super();
		this.version = version;
	}

	@SuppressWarnings("resource")
	@Override
	protected TokenStreamComponents createComponents(String fieldName,
			Reader reader) {
		final StandardTokenizer src = new StandardTokenizer(version, reader);
		src.setMaxTokenLength(maxTokenLength);
		TokenStream tok = new StandardFilter(version, src);
	    tok = new LowerCaseFilter(version, tok);
	    tok = new StopFilter(version, tok, StopAnalyzer.ENGLISH_STOP_WORDS_SET);
		// porter stemming goes here
	    tok = new SnowballFilter(src, "English");
	    
		return new TokenStreamComponents(src, tok){
			@Override
			protected void setReader(Reader reader) throws IOException {
				src.setMaxTokenLength(MyStemAnalyzer.this.maxTokenLength);
				super.setReader(reader);
			}
		};
	}

}

package sheet2.exercise9;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.misc.HighFreqTerms;
import org.apache.lucene.misc.TermStats;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.FSDirectory;

public class MySearcher {

	// Path to index directory
	public static String indexDir = "data/index";

	public static List<String> computeTopTerms(String freq, int k) {
		
		List<String> result = new ArrayList<>();
		
		if("df".equals(freq)){

			try {
				IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(indexDir)));
				TermStats[] stats = HighFreqTerms.getHighFreqTerms(reader, k, "contents", new HighFreqTerms.DocFreqComparator());
				
				for (TermStats termStat : stats) {
					result.add(termStat.termtext.utf8ToString());
				}
			} catch (Exception e) {
				System.err.println("Something went wrong: ");
				e.printStackTrace();
			}
			
		} else if("cf".equals(freq)){
			try {
				IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(indexDir)));
				TermStats[] stats = HighFreqTerms.getHighFreqTerms(reader, k, "contents", new HighFreqTerms.TotalTermFreqComparator());
				
				for (TermStats termStat : stats) {
					result.add(termStat.termtext.utf8ToString());
				}
			} catch (Exception e) {
				System.err.println("Something went wrong: ");
				e.printStackTrace();
			}
		} else {
			// runtime exception
			throw new RuntimeException("Invalid mode parameter - must be either df or cf");
		}

		return result;
	}

	// Main method for testing
	public static void main(String[] args) throws IllegalArgumentException,
			IOException, ParseException {

		List<String> topterms = MySearcher.computeTopTerms("df", 100);
		for (String term : topterms) {
			System.out.println(term);
		}
	}
}

package sheet2.exerciseA;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import edu.uci.ics.jung.algorithms.scoring.PageRank;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;

public class WebGraphGenerator {

	// LAMBDA corresponds to the notation in the lecture
	public static final double LAMBDA = 0.15d;

	// ID counter of the edges
	public static int edgeId = 0;

	// see exercise 10.3 part 1
	public Graph<String, Integer> generateWebGraph(String filename) {

		System.out.println("> GENERATION OF WEB GRAPH STARTED AT "
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new Date()));
		// initialize graph
		Graph<String, Integer> graph = new DirectedSparseGraph<>();

		// open and parse file
		System.out.println("> OPENING FILE " + filename);
		try (BufferedReader reader = Files.newBufferedReader(
				Paths.get(filename), Charset.forName("UTF-8"));) {
			while (reader.ready()) {
				// read line
				String line = reader.readLine();
				// split into source and destination
				String[] links = line.split("->");
				String source = links[0].trim();
				String dest = links[1].trim();

				// check if dest is an external link
				if (dest.contains(".edu")) {
					System.out.println(">> ADDING PAIR (" + edgeId
							+ ", SOURCE: " + source + " - DEST: " + dest
							+ ") WITH ID " + edgeId + " TO GRAPH");
					graph.addEdge(edgeId, source, dest);
					edgeId++;
				} else {
					System.out.println(">> PAIR  (SOURCE: " + source
							+ ", DEST: " + dest
							+ ") IS IGNORED - EXTERNAL LINK");
				}

			}
		} catch (IOException e) {
			System.err.println(">> ERROR: COULD NOT OPEN FILE " + filename);
			e.printStackTrace();
		}

		System.out.println("> GENERATION FINISHED AT "
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new Date()));
		return graph;

	}

	// see exercise 10.3 part 2
	public void computeAndPrintPageRanks(Graph<String, Integer> webgraph,
			int numResults, int maxItertions) {
		System.out.println("*** CREATING PAGE RANKS ***");
		// execute page rank
		PageRank<String, Integer> ranks = new PageRank<>(webgraph, LAMBDA);
		ranks.setMaxIterations(maxItertions);
		System.out.println("> PAGE RANK RUNNING - PLEASE WAIT");
		ranks.initialize();
		ranks.evaluate();
		while (!ranks.done()) {
			// wait?
		}

		System.out.println("> FINISHED");
		System.out.println("> ITERATIONS USED: " + ranks.getIterations());
		System.out.println("--- PAGE RANK RESULTS ---");
		
		// get results
		Map<String, Double> unsortedRanks = new HashMap<String, Double>();
		for (String page : webgraph.getVertices()) {
			unsortedRanks.put(page, ranks.getVertexScore(page));
		}
		
		// sort results
		TreeMap<String, Double> sortedRanks = new TreeMap<>(new ValueComparator(unsortedRanks));
		sortedRanks.putAll(unsortedRanks);
		
		// print results
		int i = 0;
		for (Entry<String, Double> entry : sortedRanks.entrySet()) {
			if(i < numResults){
				System.out.println("#" + (i+1) + " PAGE: " + entry.getKey() + " RANK: " + entry.getValue());
				i++;
			} else {
				break;
			}
		}

	}

	// Main method for testing
	public static void main(String[] args) {

		// the web graph generator
		WebGraphGenerator generator = new WebGraphGenerator();

		// filename where 'source -> dest' pairs are storend
		String filename = "linkage.txt";

		// generate the web graph
		Graph<String, Integer> webgraph = generator.generateWebGraph(filename);

		// check if a valid result is generated
		if (webgraph == null) {
			throw new RuntimeException("Work on the TODOs");
		}

		System.out.println("WebGraph is built.");

		// the desired number of results
		int numResults = 5;
		// maximum number of iterations
		int maxIterations = 10;
		// compute and print page ranks
		generator.computeAndPrintPageRanks(webgraph, numResults, maxIterations);

	}

	// helper comparator to sort by values in result map in descending order
	private class ValueComparator implements Comparator<Object> {

		Map<?, ?> map;

		public ValueComparator(Map<?, ?> map) {
			this.map = map;
		}

		@SuppressWarnings("unchecked")
		public int compare(Object keyA, Object keyB) {

			Comparable<Comparable<?>> valueB = (Comparable<Comparable<?>>) map
					.get(keyB);
			Comparable<?> valueA = (Comparable<?>) map.get(keyA);

			return valueB.compareTo(valueA);

		}
	}

}

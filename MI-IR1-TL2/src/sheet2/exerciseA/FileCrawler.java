package sheet2.exerciseA;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class FileCrawler {

	// Number of files processed.
	public int filecount = -1;

	// Number of links extracted
	public int totalLinks = -1;

	// Stores links: 'source -> dest\n' (without ')
	public StringBuilder builder = null;

	// Exercise 10.1
	public void crawl(File dir) {		
		// using JAVA NIO for recursively crawling directories/files
		Path folder = Paths.get(dir.getAbsolutePath());
		try(DirectoryStream<Path> stream = Files.newDirectoryStream(folder)){
			for(Path path : stream){
				if(Files.isDirectory(path)){
					System.out.println("* FOUND DIRECTORY: " + path.getFileName().toString());
					crawl(path.toFile());
				} else if (Files.isRegularFile(path)) {
					System.out.println("> PARSING FILE " + path.getFileName().toString());
					filecount++;
					int localLinks = extractLinks(path.toFile());
					totalLinks += localLinks;
				} else {
					// ignore element
				}
			}
		} catch (IOException e) {
			// something went wrong
			System.err.println("ERROR while reading element!");
			e.printStackTrace();
		}

	}

	// Exercise 10.2
	public int extractLinks(File file) {
		
		int numLinksOfPage = 0;

		// use document file name as source
		String source = file.getName().toString();		
		source = source.replace("_^^", "://");
		source = source.replace("^", "/");
		
		
		// load document in jsoup
		try {
			Document doc = Jsoup.parse(file, "UTF-8");
			Elements links = doc.select("a[href]");
			
			for (Element link : links) {
				String linkHref = link.attr("abs:href");
				if(linkHref.startsWith("http://") && !linkHref.contains("<") && !linkHref.contains(">") && !linkHref.contains("\n")){
					builder.append(source + " -> " + linkHref + "\n");
					numLinksOfPage++;
				}
			}
			
		} catch (IOException e) {
			System.err.println("ERROR: Could not parse file " + file.getName().toString());
			e.printStackTrace();
		}

		return numLinksOfPage;
	}

	// Main-Method for testing
	public static void main(String[] args) {

		// Name of root directory of the 4 universities datasets.
		// TODO change this path if necessary!!!
		String root = "data/webkb";
		
		
		// Crawling and extracting links.
		FileCrawler crawler = new FileCrawler();
		crawler.initialize();
		crawler.crawl(new File(root));

		// check for exercise 10.1
		if (crawler.filecount == -1 || crawler.totalLinks == -1) {
			throw new RuntimeException("Implement exercise 10.1!");
		}

		System.out.println("Processed " + crawler.filecount + " files with "
				+ crawler.totalLinks + " links.");

		// check for exercise 10.2
		if (crawler.builder == null) {
			throw new RuntimeException("Implement exercise 10.2!");
		}

		// Write result to file.
		try {
			FileWriter fw = new FileWriter("linkage.txt");
			fw.write(crawler.builder.toString());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	// helper method
	public void initialize(){
		// initialize string builder
		builder = new StringBuilder();
		// set file count to 0
		filecount = 0;
		// set link count to 0
		totalLinks = 0;
	}
}
